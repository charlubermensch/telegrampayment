.TH SP 7 "13 Mars 2022"
.SH NAME
telpay \- Run a Telegram bot to handle crypto payment
.SH SYNOPSIS
.SY "python telpay.py"
.YS
.SH CONFIG/PREPARATION
Move config files to
.I ~/.config

.RS
cp src/telpay.sh ~/.config/telpay
.br
cp src/buying.sh ~/.config/telpay_buy
.RE

Add to the first line of
.I ~/.config/telpay
your Telegram bot token given by bot father.

Edit
.I ~/.config/telpay
last line in order to change price. A number alone means that the price is in Monero. Adding a "$" at the end will make that the price will be converted to XMR at payment time.

.I ~/.config/telpay_buy
has to  be edited in case the bot handles another payment than default. (creating a Lulu print job)

.RE
.SH "REPORTING BUGS"
GitLab issues: <https://gitlab.com/charlubermensch/telegrampayment/-/issues>
.SH BUGS
If there are errors with authentication, check the config file. (\fI~/.config/telgrampayment\fP)
.SH "SEE ALSO"
python(1),https://python-telegram-bot.org/
.SH AUTHOR
Written by "Charl'Ubermensch" (pseudonyme).
.LP
.UR https://gitlab.com/charlubermensch
Remote Git repositories
.UE
.LP
.MT inlife.developper@tutanota.com
CharlUbermensch
.ME
.br
.MT contact@charlubermensch.com
CharlUbermensch
.ME
.SH COPYRIGHT
License GPLv2: GNU General Public License version 2.
<https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>
.PP
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
