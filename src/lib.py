"""Pure functions on any topic."""

import sys

from os import environ

from subprocess import getstatusoutput

from telegram import Update

from src.config import get_config


def get_hook_var() -> list[str]:
    """Get `~/.config/telpay_buy` hook variables."""
    with open("%s/.config/telpay_buy" % environ["HOME"], "rb") as file:
        s_file = str(file.read(), encoding="utf-8")
    words = s_file.split(" ")
    filters_set = {w for w in words if w[0] == "%"}  # set()
    return list(filters_set)


def get_replaced_hook(var: dict):
    """Get `~/.config/telpay_buy` with variables replaced."""
    if len(var) != len(get_hook_var()):
        print("warn: len(var) != get_hook_var()", file=sys.stderr)

    hook_path = "%s/.config/telpay_buy" % environ["HOME"]

    with open(hook_path, "rb") as file:
        s_file = str(file.read(), encoding="utf-8")

    for key in var:
        s_file = s_file.replace(key, var[key])

    return s_file


def manage_crash(out: str, update: Update):
    """Respond with `update` `get_crash_msg(out)`."""
    msg = get_crash_msg(out)
    update.message.reply_text("😵 " + msg)  # 💀
    sys.exit(1)


def get_crash_msg(out: str):
    """Return a crash message according to `out`."""
    if out == "error: no empty address has been found nor can be made":
        msg = "the payment processor is overwhelmed"
        print(msg, file=sys.stderr)
        return msg

    err = "\"%s\"" % repr(out) if out.count('\n') < 2 else out

    print(err, file=sys.stderr)
    return "unknown crash occured internally"


def get_incoming(update: Update, address: str) -> float:
    """Return "total balance" of `address`."""
    cmd = get_config()[2]  # sp get

    code, out = getstatusoutput(cmd + " " + address)

    if code != 0 or not out:
        manage_crash(out, update)

    return float(out.strip())


def get_unlocked(update: Update, address: str) -> float:
    """Return "unlocked balance" of `address`."""
    cmd = get_config()[3]  # sp get --unlocked

    code, out = getstatusoutput(cmd + " " + address)

    if code != 0 or not out:
        manage_crash(out, update)

    return float(out.strip())


def get_diff_emoji_inequal(paid: float, price: float) -> str:
    """Return emoji when `paid` != `price`."""
    if paid == 0:
        return "⏳"

    if paid < price:
        return "⌛"  # ➖

    return "📈"  # ➕, ⚠️ , 🚨


def get_diff_emoji(paid: float, price: float) -> str:
    """Return emoji for `def_get_msg()`."""
    if paid != price:
        return get_diff_emoji_inequal(paid, price)

    return "✅"  # 👌


def get_diff_msg(paid: float, price: float) -> str:
    """Return appropriate difference message."""
    emoji = get_diff_emoji(paid, price)

    return "{} {}/{} XMR".format(emoji, paid, price)


def get_is_confirmed(update: Update, address: str) -> bool:
    """Return if there is no incoming payments."""
    incoming = get_incoming(update, address)
    unlocked = get_unlocked(update, address)

    return incoming == unlocked


def get_clean_lines(text: str) -> list:
    """
        Return clean lines parsed from `text`.

        Sub-function of `get_infos()`.

        Return an empty list in case there is issue with `text`.
    """
    dirty_lines = text.split("\n")

    if dirty_lines[0] != "/start":
        return []

    lines = [line for line in dirty_lines if line.strip()]

    if len(lines) < 8 or len(lines) > 9:
        return []

    return lines


def get_address(lines: str) -> dict:  # noqa
    """Return address as dict from `lines`."""
    address = {}

    keys = (
        "city",
        "country_code",
        "name",
        "phone_number",
        "postcode",
        "street1",
        "state_code"
    )

    for i, line in enumerate(lines):
        address[keys[i]] = line

    if len(address) < 6:
        return {}  # wtf

    if "state_code" not in address:
        address["state_code"] = ""  # optional

    return address


def get_infos(text: str) -> list:
    """
        Return infos parsed from `text`.

        [email: str, address: dict]

        Part of a very dirty code.

        Return empty list in case of syntax issue.
    """
    lines = get_clean_lines(text)
    if not lines:
        return []

    email = lines[1]

    address = get_address(lines[2:])
    if not address:
        return []

    return [email, address]
