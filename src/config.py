"""Pure functions returning `~/.config/telpay` informations()."""

import sys

from os import environ
from os.path import isfile

from subprocess import getstatusoutput


WAIT_TIME = 0.3


def get_raw_config() -> str:
    """Return config as str."""
    config_path = "%s/.config/telpay" % environ["HOME"]

    if not isfile(config_path):
        print("error: no config file", file=sys.stderr)
        sys.exit(1)

    with open(config_path, "rb") as file:
        b_file = file.read()
    return str(b_file, encoding="utf-8")


def get_trailingless(list_: list) -> list:
    """Return `list_` without a trailing last empty str."""
    list_ = [e for e in list_ if e]

    if len(list_) == 6:
        return list_

    msg = "too short" if len(list_) < 6 else "too long"
    print("error: config file %s" % msg, file=sys.stderr)
    sys.exit(1)


def get_config() -> list:
    """Return config as list."""
    raw_config = get_raw_config()
    return get_trailingless(raw_config.split("\n"))


def get_token() -> str:
    """Return token from config."""
    return get_config()[0]


def get_converted_to_xmr(usd: float) -> float:
    """Return `usd` price converted to XMR."""
    code, output = getstatusoutput("curl -s 'https://usd.rate.sx/1xmr'")

    if code != 0:
        print("error: failed to get XMR rate", file=sys.stderr)
        print(output, file=sys.stderr)
        sys.exit(1)

    # /!\ Division by zero or ValueError

    xmr_to_usd = float(output)  # one XMR = $xmr_to_usd dollars

    usd_to_xmr = 1 / xmr_to_usd  # one usd = $usd_to_xmr euros

    raw_price = usd_to_xmr * usd

    return round(raw_price, 3)


def get_price() -> float:
    """Return price from config."""
    s_price = get_config()[5]
    if s_price[-1] != "$":
        return float(s_price)
    return get_converted_to_xmr(float(s_price[:-1]))
