"""File with impure functions."""

import sys

from json import dumps

from subprocess import getstatusoutput

from threading import Event

from telegram import Update

from src.config import get_config, get_price, WAIT_TIME

from src.lib import (
    get_incoming,
    get_diff_msg,
    manage_crash,
    get_is_confirmed,
    get_unlocked,
    get_infos,
    get_replaced_hook
)


def execute_fileless(update: Update, hook_file: str):
    """Execute `hook_file` line per line with `subprocess.getstatusoutput`."""
    for line in hook_file.split("\n"):
        print("\"%s\"" % line)

        code, output = getstatusoutput(line)

        print("\"%s\"" % output)

        if code != 0:
            print("error: failed to execute hook", file=sys.stderr)
            msg = "😶 The post-payment action (sending the product, ...) has failed"
            update.message.reply_text(msg)
            msg2 = "😌 Keep calm: your payment is 100% proven"
            update.message.reply_text(msg2)
            sys.exit(0)  # do not clean the address -> proof


def execute_hook(update: Update, infos: list):
    """Execute `~/.config/telpay_buy` hook."""
    var_values = {
        "%EMAIL": infos[0],
        "%ADDRESS_INFO": dumps(infos[1])  # str()
    }
    hook_file = get_replaced_hook(var_values)
    print(f"{hook_file=}")
    execute_fileless(update, hook_file)


def make_clean(update: Update, address: str):
    """Clean the payment processor after the payment."""
    cmd = get_config()[4]  # sp del

    code, out = getstatusoutput(cmd + " " + address)

    if code != 0:
        manage_crash(out, update)

    update.message.reply_text("♻️ post-payment cleaning has been sucessful")


def confirm_payment(update: Update, address: str, infos: list):
    """Execute hook and clean the payment processor."""
    execute_hook(update, infos)
    make_clean(update, address)


def start_(*args):
    """Call `start()` but don't crash on `SystemExit`."""
    try:
        start(*args)
    except SystemExit:
        pass


def send_diff(update: Update, paid: float, price: float):
    """Communicate the difference between `new` and `paid`."""
    msg = get_diff_msg(paid, price)
    update.message.reply_text(msg)


def check_incoming(update: Update, address: str, infos: list):
    """Check incoming payment by checking incoming balance."""
    paid = 0
    price = infos[2]
    while paid < price:
        Event().wait(timeout=WAIT_TIME)
        new_paid = get_incoming(update, address)
        if new_paid != paid:
            send_diff(update, new_paid, price)
        paid = new_paid


def check_confirmed(update: Update, address: str):
    """Wait until `get_is_all_unlocked()` is `True` and say it."""
    update.message.reply_text("⏲️  Payments are confirming. Wait 20-25 minutes.")

    confirmed = False
    while not confirmed:
        confirmed = get_is_confirmed(update, address)


def get_address(update: Update, infos: list) -> str:
    """Return and send payment subaddress."""
    start_cmd = get_config()[1]

    code, out = getstatusoutput(start_cmd)

    if code != 0 or not out:
        manage_crash(out, update)

    update.message.reply_text("🔢 Payment amount: (in XMR)")
    update.message.reply_text("%s" % infos[2])

    update.message.reply_text("📍 Payment address:")
    update.message.reply_text(out)  # alone message help with selecting

    return out


def check_last(update: Update, address: str, infos: list):
    """Check a last time that the money unlocked is good."""
    price = infos[2]
    unlocked = get_unlocked(update, address)

    if unlocked < price:  # wtf
        update.message.reply_text("🚨 Probably a bug: XMR disapppeared from confirmed")
        sys.exit(1)

    if unlocked > price:
        update.message.reply_text("⚠️  You've paid too much, you may ask to get repaid.")
        return

    update.message.reply_text("👌 Payment confirmed")


def get_other_info(update: Update) -> list:
    """
        Return email & address found in `update.message` with price.

        -> [email: str, address: dict, price: float].
    """
    msg = update.message

    text = getattr(msg, "text", "")

    if not text or "/start" not in text:
        update.message.reply_text("🤔 the bot has bugs that make no sense")
        sys.exit(0)

    parsed = get_infos(text)

    if not parsed:
        update.message.reply_text("ℹ️  for your information the format requested to order is:")
        msg = "/start\nemail\ncity\ncountry code\nfull name\nphone number\npostcode\nstreet\nstate code(optional)"  # noqa
        update.message.reply_text(msg)
        sys.exit(0)

    return parsed + [get_price()]


def start(update: Update):
    """React to `/start`."""
    infos = get_other_info(update)

    address = get_address(update, infos)

    check_incoming(update, address, infos)
    check_confirmed(update, address)
    check_last(update, address, infos)

    confirm_payment(update, address, infos)
