"""Manage payments using Telegram."""

import sys

from os import environ
from os.path import isfile

from telegram.ext import Updater, CommandHandler

from src.unpure import start_

from src.config import get_token


def run():
    """Launch the bot."""
    if not isfile(environ["HOME"] + "/.config/telpay_buy"):
        print("error: ~/.config/telpay_buy not found", file=sys.stderr)
        sys.exit(1)

    updater = Updater(get_token())

    sa = lambda u, c: start_(u)  # noqa

    updater.dispatcher.add_handler(CommandHandler('start', sa))

    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    run()
