<div align="center">

<img src="https://gitlab.com/uploads/-/system/project/avatar/33559129/telpay_gitlab.jpg" width="200">

# TelegramPayment

[![License: GPL v2](https://img.shields.io/badge/License-GPL_v2-blue.svg)](https://tldrlegal.com/license/gnu-general-public-license-v2)

A simple [Telegram](https://telegram.org) bot front-end for [SucklessPayment](https://gitlab.com/charlubermensch/suckless_payment).

[Installation](#installation) •
[Getting started](#getting-started) •
[Further](#further)

</div>

<img src="https://charlubermensch.com/images/telpay.gif" width="400"/>

(UX has changed since, this gotta be updated)

# Installation

As of now, only through this gitlab:

```
git clone https://gitlab.com/charlubermensch/telegrampayment.git
cd telegrampayment
```

Install the depency

```
pip install python-telegram-bot
```

Move config files to `~/.config`

```
cp src/telpay.sh ~/.config/telpay
cp src/buying.sh ~/.config/telpay_buy
```

# Getting started

Put your token at the beginning of `~/.config/telpay`. (at the empty line)

Launch the bot with this command

```
python telpay.py
```

Change the last line of `~/.config/telpay` in order to change the minimum amount that has to be paid. Now this amount can be given in the stably declining money US dollar (other currency aren't integrated for minimalism) by writing "$" at the end. The price is rounded to the **third decimal**, e.g. 0.0867 -> 0.087. It changes around 0.08$ the price maximum. If this change goes beyond the fees, the decimal may become greater (fourth, fifth, ...). However the software is still not indented for small transacton, i.e. < 5-8€, since the fees of Monero are fix anyway.

According to the evolution https://gitlab.com/charlubermensch/telegrampayment/-/issues/1, you may change `~/.config/telpay_buy` in the future.

# Further

## Code base

To help to remove functions sizes, the author uses `sys.exit(0)` when a command is finished to be traited. This does not clutter the software stderr/stdout.

`Event().wait(timeout=0.1)` ([1](https://stackoverflow.com/questions/3416160/issues-with-time-sleep-and-multithreading-in-python#3420934)) is used for waiting instead of `time.sleep(0.1)` because the module used uses the `threading` module. ([2](https://github.com/python-telegram-bot/python-telegram-bot/wiki/Performance-Optimizations#multithreading)) This may be be called into question since the project uses `time.sleep()` internally. ([3](https://github.com/python-telegram-bot/python-telegram-bot/wiki/Avoiding-flood-limits#available-tools-and-their-implementation-details))

It may be better than commands run in multithread. [4](https://github.com/python-telegram-bot/python-telegram-bot/wiki/Performance-Optimizations#how-to-use-it)

`start_` function in `src/unpure.py` is used to avoid that the software totally crashs when using `sys.exit(0)` and however still being able to use it.

Logging is pretty bloat, especially by opening a file. It's why one can prefer to `print()` detailled errors on `sys.stderr`. Traceback is already provided by Python anyway.

As of now, the bot doesn't check the output of the payment processor nor TOKEN. It may be added hereafter with regex, e.g. testing that the balance returned is float 0.

The parsing of `~/.config/telpay_buy` is really temporary so much it is badly written. As of now, it splits the document in words (including within quotation marks) and consider a variable a word that starts "%", even if there is a "\" before.

`curl -s "usd.rate.sx/1xmr"` is inspired from [Luke Smith](https://lukesmith.xyz) Void Rice [sb-price script](https://github.com/LukeSmithxyz/voidrice/blob/master/.local/bin/statusbar/sb-price).

N.B: this section is used to avoid comments which are despised by most "code clean" views

## Bugs

`the payment processor is overwhelmed` means that [sp](https://gitlab.com/charlubermensch/suckless_payment) has no unused addresses left.

Fix: increasing the level of sub-addresses (no cost known yet) or increasing transaction fee in order that the addresses are empty faster (cost more)

## Roadmap

- [x] make a draft / proof of concept
- [x] refactor (rewrite everything)
- [x] add payment hook
- [ ] deleting subaddress: read warnings

## Authors and acknowledgment

As of now, only [Charl'Übermensch](https://charlubermensch.com)

## License

This code is licensed under the GNU General Public License version 2 or later. (SPDX: GPL-2.0-or-later)

Reuse of the code is possible under GPLv3 (or later) but the author is willing to use the GPLv2 for the software itself.

This is free software: you are fre eto change and redistribute it. There is NO WARRANTY, to the extent permitted by law.

The module which the software is based upon has for him a [BSD 3-Clause License](https://monero-python.readthedocs.io/en/latest/license.html).
